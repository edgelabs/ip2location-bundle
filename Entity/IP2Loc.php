<?php

namespace EdgeLabs\IP2LocationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * IP2Loc
 *
 * @ORM\Table(name="ip2location", indexes={
 *      @ORM\Index(name="ip2loc_from", columns={"ip_from"}),
 *      @ORM\Index(name="ip2loc_to", columns={"ip_to"}),
 *      @ORM\Index(name="ip2loc_from_to", columns={"ip_from", "ip_to"}),
 * })
 * @ORM\Entity(repositoryClass="EdgeLabs\IP2LocationBundle\Entity\IP2LocRepository")
 */
class IP2Loc
{
    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="ip_from", type="float", nullable=false)
     */
    private $ipFrom;

    /**
     * @var float
     *
     * @ORM\Column(name="ip_to", type="float", nullable=false)
     */
    private $ipTo;

    /**
     * @var string
     *
     * @ORM\Column(name="country_code", type="string", length=2, nullable=false)
     */
    private $countryCode;

    /**
     * @var string
     *
     * @ORM\Column(name="country_name", type="string", length=64, nullable=false)
     */
    private $countryName;

    /**
     * @var string
     *
     * @ORM\Column(name="locale", type="string", length=4, nullable=true)
     */
    private $locale;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ipFrom
     *
     * @param float  $ipFrom
     * @param string $type [ip2long|raw]
     *
     * @return \EdgeLabs\IP2LocationBundle\Entity\IP2Loc
     */
    public function setIpFrom($ipFrom, $type = 'ip2long')
    {
        $this->ipFrom = ($type === 'ip2long' ? $ipFrom : ip2long($ipFrom));
        return $this;
    }

    /**
     * Get ipFrom
     *
     * @return float 
     */
    public function getIpFrom()
    {
        return $this->ipFrom;
    }

    /**
     * Set ipTo
     *
     * @param float $ipTo
     * @param string $type [ip2long|raw]
     *
     * @return IP2Loc
     */
    public function setIpTo($ipTo, $type = 'ip2long')
    {
        $this->ipTo = ($type === 'ip2long' ? $ipTo : ip2long($ipTo));
        return $this;
    }

    /**
     * Get ipTo
     *
     * @return float 
     */
    public function getIpTo()
    {
        return $this->ipTo;
    }

    /**
     * Set countryCode
     *
     * @param string $countryCode
     * @return IP2Loc
     */
    public function setCountryCode($countryCode)
    {
        $this->countryCode = $countryCode;

        return $this;
    }

    /**
     * Get countryCode
     *
     * @return string 
     */
    public function getCountryCode()
    {
        return $this->countryCode;
    }

    /**
     * Set countryName
     *
     * @param string $countryName
     * @return IP2Loc
     */
    public function setCountryName($countryName)
    {
        $this->countryName = $countryName;

        return $this;
    }

    /**
     * Get countryName
     *
     * @return string 
     */
    public function getCountryName()
    {
        return $this->countryName;
    }

    /**
     * Get $this->locale
     *
     * @return string
     */
    public function getLocale() {
        return $this->locale;
    }

    /**
     * Set $this->locale
     *
     * @param string $locale
     */
    public function setLocale($locale) {
        $this->locale = $locale;
    }
}
