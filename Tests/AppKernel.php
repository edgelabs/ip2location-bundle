<?php

use Symfony\Component\Filesystem\Filesystem;

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

/**
 * Class AppKernel
 *
 * @author Steve Todorov <steve.todorov@carlspring.com>
 */
class AppKernel extends Kernel
{
    /**
     * {@inheritdoc}
     */
    public function registerBundles()
    {
        $bundles = array(
            new \Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Liip\FunctionalTestBundle\LiipFunctionalTestBundle(),
            new \EdgeLabs\IP2LocationBundle\IP2LocationBundle()
        );

        return $bundles;
    }

    /**
     * {@inheritdoc}
     */
    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load(__DIR__.'/config/config_'.$this->getEnvironment().'.yml');
    }

    /**
     * {@inheritdoc}
     */
    public function getCacheDir() {
        return sys_get_temp_dir().'/IP2LocationBundle/cache';
    }

    /**
     * {@inheritdoc}
     */
    public function getLogDir() {
        return sys_get_temp_dir().'/IP2LocationBundle/logs';
    }
}