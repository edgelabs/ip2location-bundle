<?php

namespace EdgeLabs\IP2LocationBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class TestController
 *
 * @author  Steve Todorov <steve.todorov@carlspring.com>
 * @package EdgeLabs\IP2LocationBundle\Tests\Integration\EventListener
 */
class TestController extends Controller
{

    public function welcome($locale = null) {
        return new Response('welcome page'.($locale ? ' '.$locale : null));
    }


    public function login() {
        return new Response('login page');
    }
}

?>
