<?php

namespace EdgeLabs\IP2LocationBundle\Tests\Fixtures;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use EdgeLabs\IP2LocationBundle\Entity\IP2Loc;

/**
 * @author Steve Todorov <steve.todorov@carlspring.com>
 */
class LoadIPData implements FixtureInterface
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager) {
        $bg = new IP2Loc();
        $bg->setIpFrom('194.153.145.0', 'raw');
        $bg->setIpTo('194.153.145.255', 'raw');
        $bg->setCountryCode('BG');
        $bg->setCountryName('Bulgaria');
        $bg->setLocale('bg');
        $manager->persist($bg);
        $manager->flush();

        $fa = new IP2Loc();
        $fa->setIpFrom('2.146.96.0', 'raw');
        $fa->setIpTo('2.146.127.255', 'raw');
        $fa->setCountryCode('IR');
        $fa->setCountryName('Islamic Republic of Iran');
        $fa->setLocale('fa');
        $manager->persist($fa);
        $manager->flush();
    }
}

?>
