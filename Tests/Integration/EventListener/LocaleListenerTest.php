<?php

namespace EdgeLabs\IP2LocationBundle\Tests\Integration\EventListener;

use Doctrine\Bundle\DoctrineBundle\Command\Proxy\UpdateSchemaDoctrineCommand;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\SchemaTool;
use EdgeLabs\IP2LocationBundle\Entity\IP2Loc;

use Liip\FunctionalTestBundle\Test\WebTestCase;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Bundle\FrameworkBundle\Client;

/**
 * Class LocaleListenerTest
 *
 * @author  Steve Todorov <steve.todorov@carlspring.com>
 * @package EdgeLabs\IP2LocationBundle\Tests\EventListener
 */
class LocaleListenerTest extends WebTestCase {

    /**
     * @var Client
     */
    protected $client;

    /**
     * @var ContainerInterface
     */
    protected $container;

    protected $ips;

    public function setUp()
    {
        $this->client = static::createClient();
        $this->container = $this->client->getContainer();
        $this->ips = $this->container->getParameter('ip2location.ips');
    }

    public function testRequest()
    {
        $this->loadFixtures(array('EdgeLabs\IP2LocationBundle\Tests\Fixtures\LoadIPData'));

        // Test if LocaleListener is redirecting to the proper url based on the IP2Location country.
        foreach($this->ips as $address) {
            $this->client->setServerParameter('REMOTE_ADDR', trim($address['ip']));

            $this->client->request('GET', '/');

            $response = $this->client->getResponse();
            $this->assertTrue($response->isRedirection(), "Locale listener is not redirecting at all!");
            $this->assertTrue($response->isRedirect('/'.$address['locale'].'/'), "Locale listener is not to the proper location for ".$address['ip']."/".$address['locale']."!");
            $this->assertEquals($address['locale'], $this->client->getContainer()->get('session')->get('_locale', null), 'Expected session to have _locale key with value ['.$address['locale'].'] but got '.$this->client->getContainer()->get('session')->get('_locale', null));

            $this->client->restart();
        }
    }

    public function testRequestRevisiting()
    {
        $this->loadFixtures(array('EdgeLabs\IP2LocationBundle\Tests\Fixtures\LoadIPData'));

        // Test if LocaleListener is redirecting to the proper url based on the IP2Location country.
        foreach($this->ips as $address) {
            $this->client->setServerParameter('REMOTE_ADDR', trim($address['ip']));

            // First time visit saves locale in session.
            $this->client->request('GET', '/');

            // Second time visit should have locale stored in session.
            $this->client->request('GET', '/');

            $response = $this->client->getResponse();
            $this->assertEquals($address['locale'], $response->headers->get('X-Local-Cache', null), "Expected \"".$address['locale']."\" locale in session for ip ".$address['ip'].", got something else!");
            $this->assertEquals($address['locale'], $response->headers->get('X-Locale', null));
            $this->assertEquals($address['locale'], $this->client->getContainer()->get('session')->get('_locale', null), 'Expected session to have _locale key with value ['.$address['locale'].'] but got '.$this->client->getContainer()->get('session')->get('_locale', null));

            $this->client->restart();
        }
    }

    public function testRequestWithNoLocaleInURI()
    {
        $this->loadFixtures(array('EdgeLabs\IP2LocationBundle\Tests\Fixtures\LoadIPData'));

        // Test if LocaleListener is redirecting to the proper url based on the IP2Location country.
        foreach($this->ips as $address) {
            $this->client->setServerParameter('REMOTE_ADDR', trim($address['ip']));

            $this->client->request('GET', '/login');

            $response = $this->client->getResponse();
            $this->assertEquals('false', $response->headers->get('X-Local-Cache', null), "Expected \"".$address['locale']."\" locale in session for ip ".$address['ip'].", got something else!");
            $this->assertEquals($address['locale'], $response->headers->get('X-Locale', null));
            $this->assertEquals($address['locale'], $this->client->getContainer()->get('session')->get('_locale', null), 'Expected session to have _locale key with value ['.$address['locale'].'] but got '.$this->client->getContainer()->get('session')->get('_locale', null));

            $this->client->restart();
        }
    }

    public function testRequestLocaleInURI()
    {
        $this->loadFixtures(array('EdgeLabs\IP2LocationBundle\Tests\Fixtures\LoadIPData'));

        // Test if LocaleListener is redirecting to the proper url based on the IP2Location country.
        foreach($this->ips as $address) {
            $this->client->setServerParameter('REMOTE_ADDR', trim($address['ip']));

            $forcedLocale = 'bg';

            $this->client->request('GET', '/'.$forcedLocale);

            $response = $this->client->getResponse();
            $this->assertEquals('false', $response->headers->get('X-Local-Cache', null), "Expected \"".$address['locale']."\" locale in session for ip ".$address['ip'].", got something else!");
            $this->assertEquals($forcedLocale, $response->headers->get('X-Locale', null));
            $this->assertEquals($forcedLocale, $this->client->getContainer()->get('session')->get('_locale', null), 'Expected session to have _locale key with value ['.$address['locale'].'] but got '.$this->client->getContainer()->get('session')->get('_locale', null));

            $this->client->restart();
        }
    }

    public function testInvalidLocaleInURI() {
        $this->loadFixtures(array('EdgeLabs\IP2LocationBundle\Tests\Fixtures\LoadIPData'));

        // Test if LocaleListener is redirecting to the proper url based on the IP2Location country.
        foreach($this->ips as $address) {
            $this->client->setServerParameter('REMOTE_ADDR', trim($address['ip']));

            // First time visiting invalid locale URI should fallback to "default" locake.
            $this->client->request('GET', '/js/');

            // Second time visit should have locale stored in session.
            $this->client->request('GET', '/');

            $response = $this->client->getResponse();
            $this->assertEquals($address['locale'], $response->headers->get('X-Local-Cache', null), "Expected \"".$address['locale']."\" locale in session for ip ".$address['ip'].", got something else!");
            $this->assertEquals($address['locale'], $response->headers->get('X-Locale', null));
            $this->assertEquals($address['locale'], $this->client->getContainer()->get('session')->get('_locale', null), 'Expected session to have _locale key with value ['.$address['locale'].'] but got '.$this->client->getContainer()->get('session')->get('_locale', null));

            $this->client->restart();
        }
    }

}