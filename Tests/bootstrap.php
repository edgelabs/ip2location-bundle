<?php

error_reporting(error_reporting() & ~E_USER_DEPRECATED);

use Doctrine\Common\Annotations\AnnotationRegistry;
use Composer\Autoload\ClassLoader;

$vendorDir = __DIR__.'/../vendor';

/**
 * @var ClassLoader $loader
 */
$loader = require $vendorDir.'/autoload.php';
AnnotationRegistry::registerLoader(array($loader, 'loadClass'));

if (PHP_VERSION_ID >= 50400 && gc_enabled()) {
    // Disabling Zend Garbage Collection to prevent segfaults with PHP5.4+
    // https://bugs.php.net/bug.php?id=53976
    gc_disable();
}

return $loader;
