<?php

namespace EdgeLabs\IP2LocationBundle\EventListener;

use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class LocaleListener
 *
 * @author  Steve Todorov <steve.todorov@carlspring.com>
 * @package EdgeLabs\IP2LocationBundle\EventListener
 */
class LocaleListener implements EventSubscriberInterface
{

    /**
     * @var Container
     */
    private $container;

    /**
     * @var string
     */
    private $defaultLocale;

    /**
     * @var array
     */
    private $allowedLocales;

    private $override = false;

    private $cachedLocale = null;
    private $locale = null;

    private $redirect = false;

    public function __construct(Container $container, $defaultLocale = 'en', $allowedLocales = null, $override = false)
    {
        $this->container      = $container;
        $this->defaultLocale  = $defaultLocale;
        $this->allowedLocales = $allowedLocales;
        $this->override       = $override;

        if(is_string($this->allowedLocales)) {
            $this->allowedLocales = explode('|', $allowedLocales);
        }
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        if($this->override) {
            $request = $event->getRequest();

            $uri = $request->getRequestUri();

            if (!$uri || $uri == '/') {
                if ($locale = $this->container->get('session')->get('_locale', null)) {
                    $this->redirect = true;
                    $this->cachedLocale = $locale;
                }
                else
                {
                    $this->redirect = true;
                    $locale = $this->dbLookup($event);
                }
            }
            else if (preg_match('/^(\/*)?('.implode('|', $this->allowedLocales).')(\/*)?/i', $uri, $matched)) {
                $locale = $matched[2];
            }
            // Make sure this is set as fallback.
            else {
                // If we already have the locale stored in the session use it.
                $locale = $this->container->get('session')->get('_locale', null);

                // Try to guess the locale, if we can't find it stored in the session.
                // This is useful when we're trying to access a url without a locale in the path (i.e. /login)
                // in which case we don't need the redirect.
                if (!$locale) {
                    $locale = $this->dbLookup($event);
                }
            }

            // Set locale system-wide
            $request->getSession()->set('_locale', $locale);
            $request->setLocale($locale);
            $this->locale = $locale;
        }
    }

    public function onKernelResponse(FilterResponseEvent $event)
    {
        if($this->override && $this->locale) {

            // Make sure we store the _locale into the session upon kernel response.
            $this->container->get('session')->set('_locale', $this->locale);

            if($this->redirect) {
                $response = new RedirectResponse('/'.$this->locale.'/', 302);
                $response->setExpires(new \DateTime());
                $response->setPrivate();
                $response->headers->addCacheControlDirective('must-revalidate', true);
                $this->addDebugHeaders($response, $this->cachedLocale);

                $event->setResponse($response);
            } else {
                $response = $event->getResponse();
                $this->addDebugHeaders($response);

                $event->setResponse($response, $this->cachedLocale);
            }
        }
    }

    private function dbLookup(GetResponseEvent $event) {
        $request = $event->getRequest();

        $em = $this->container->get('doctrine');

        $repo = $em->getRepository('IP2LocationBundle:IP2Loc');

        $locale = null;
        $result = null;
        $ip = $request->getClientIp();

        if($ip){
            $query = $repo->findByIp($ip);
            try {
                $result = $query->getSingleResult();
                $locale = strtolower($result->getLocale());
            }
            catch(NoResultException $e) {}
            catch(NonUniqueResultException $e) {}
        }

        if(!in_array($locale, $this->allowedLocales)) {
            $locale = $this->defaultLocale;
        }

        return $locale;
    }

    private function addDebugHeaders(&$response, $cached = 'false')
    {
        if(in_array($this->container->get('kernel')->getEnvironment(), array('test', 'dev'))) {
            $response->headers->set('X-Local-Cache', $cached);
            $response->headers->set('X-Locale', $this->locale);
        }
    }

    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * The array keys are event names and the value can be:
     *
     *  * The method name to call (priority defaults to 0)
     *  * An array composed of the method name to call and the priority
     *  * An array of arrays composed of the method names to call and respective
     *    priorities, or 0 if unset
     *
     * For instance:
     *
     *  * array('eventName' => 'methodName')
     *  * array('eventName' => array('methodName', $priority))
     *  * array('eventName' => array(array('methodName1', $priority), array('methodName2'))
     *
     * @return array The event names to listen to
     *
     * @api
     */
    public static function getSubscribedEvents() {
        return array(
            // must be registered before the default Locale listener
            KernelEvents::REQUEST => array(array('onKernelRequest', 17)),
            KernelEvents::RESPONSE => array(array('onKernelResponse', 16)),
        );
    }
}
?>
