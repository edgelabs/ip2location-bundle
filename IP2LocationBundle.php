<?php

namespace EdgeLabs\IP2LocationBundle;

use EdgeLabs\IP2LocationBundle\DependencyInjection\IP2LocationExtension;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class IP2LocationBundle extends Bundle {

    public function getContainerExtension()
    {
        if (null === $this->extension) {
            $this->extension = new IP2LocationExtension();
        }

        return $this->extension;
    }
}
