<?php

namespace EdgeLabs\IP2LocationBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;
use Symfony\Component\Config\Definition\Exception\InvalidTypeException;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();

        $rootNode = $treeBuilder->root('ip2location');

        $rootNode
            ->children()
                ->booleanNode('override_root_page')
                    ->defaultTrue()
                ->end()
                ->scalarNode('fallback_locale')
                    ->defaultValue('%locale%')
                ->end()
                ->variableNode('source')
                    ->defaultValue('%kernel.root_dir%/Resources/third-party/ip2location/')
                    ->validate()
                    ->always(function ($v) {
                        if (is_string($v) || is_array($v)) {
                            if(is_string($v)) {
                                $v = array($v);
                            }
                            return $v;
                        }
                        throw new InvalidTypeException();
                    })
                ->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
