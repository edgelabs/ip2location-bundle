Installation
============

Step 1: Copy the bundle into your src/
--------------------------------------

Open up your file explorer and copy the bundle into your project.

Step 2: Enable the bundle
-------------------------

Then, enable the bundle by adding it to the list of registered bundles
in the ``app/AppKernel.php`` file of your project:

    <?php
    // app/AppKernel.php

    // ...
    class AppKernel extends Kernel
    {
        public function registerBundles()
        {
            $bundles = array(
                // ...

                new EdgeLabs\IP2LocationBundle\CMSIP2LocationBundle(),
            );

            // ...
        }

        // ...
    }

Step 3: Download the IP2Location database
-----------------------------------------

Go to [ip2location.com](http://lite.ip2location.com/database-ip-country#ipv4) and download the latest csv version of the database.

Step 4: Initialize the database:
--------------------------------

In your ``app/config/config.yml`` set the path to the downloaded file:

    ip2location:
        fallback_locale: en (default is %locale%
        locales: [bg, ir, en]
        override_root_page: false  # If set to true, will redirect "/" to "/LOCALE/" based on user IP. 
        source:
            - "%kernel.root_dir%/Resources/ip2location/country-ip-blocks-countryName.csv"
            - "%kernel.root_dir%/Resources/ip2location/IP2LOCATION-LITE-DB1.CSV"

Then update your database schema (BE CAREFUL ON PRODUCTION SERVERS!) 

    php app/console doctrine:schema:update --force

Now load the csv file into the database by executing

    php app/console cms:ip2location:load


CSV file format:
----------------
 
The CSV file must be in the following format:
 
    SUBNET_START  SUBNET_END   COUNTRY CODE    COUNTRY NAME   LOCALE
    5.10.64.64,   5.10.64.127, BG,             Bulgaria,      bg

The IP addresses will be converted to ip2long.

Versions:
----------

 ``v1.x`` works with Symfony 2.x
 ``v2.x`` works with Symfony 3.0.x