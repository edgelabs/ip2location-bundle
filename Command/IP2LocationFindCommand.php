<?php

namespace EdgeLabs\IP2LocationBundle\Command;

use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use EdgeLabs\IP2LocationBundle\Entity\IP2Loc;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Helper\ProgressHelper;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @Author:  Steve Todorov <steve.todorov@carlspring.com>
 */
class IP2LocationFindCommand extends ContainerAwareCommand {

    public function configure() {
        $this
            ->setName('ip2location:find')
            ->setDescription('Lookup if an ip can be matched')
            ->addArgument(
                'ip',
                InputOption::VALUE_REQUIRED,
                'IP address to lookup (i.e. 127.0.0.1; writing "internet" will match againt the current public IP address)'
            )
            ->addOption('debug')
        ;
    }

    public function execute(InputInterface $input, OutputInterface $output) {

        $ip = $input->getArgument('ip');
        $debug = $input->getOption('debug');

        if($ip == 'internet') {
            $ip = trim(file_get_contents('http://ipecho.net/plain'));
        }

        $query = $this->getContainer()->get('doctrine')->getRepository('IP2LocationBundle:IP2Loc')->findByIp($ip);

        try {
            $result = $query->getSingleResult();
            $output->write("\nIP address ".$ip." is from ".$result->getCountryName());
        } catch(NoResultException $e){
            $output->write("\nIP address ".$ip." could not be matched.");
        } catch(NonUniqueResultException $e){
            $results = $query->getResult();

            $table = new Table($output);
            $table->setHeaders(array('From', 'To', 'Code', 'Country'));

            /**
             * @var IP2Loc $result
             */
            foreach($results as $result){
                $table->addRow(array($result->getIpFrom(), $result->getIpTo(), $result->getCountryCode(), $result->getCountryName()));
            }

            $table->render();
        }

        $output->write("\n\n");

        if($debug){
            $output->write("SQL: ".$query->getSQL()."\n");
        }
    }

}
