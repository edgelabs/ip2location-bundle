<?php

namespace EdgeLabs\IP2LocationBundle\Command;

use EdgeLabs\IP2LocationBundle\Entity\IP2Loc;
use SplFileObject;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Helper\ProgressHelper;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Finder\Finder;

/**
 * Class IP2LocationLoadCommand
 *
 * @author  Steve Todorov <steve.todorov@carlspring.com>
 * @package EdgeLabs\IP2LocationBundle\Command
 */
class IP2LocationLoadCommand extends ContainerAwareCommand {

    /**
     * @var RegistryInterface
     */
    private $doctrine = null;

    private $flush = false;

    private $ip2long = false;

    public function configure() {
        $this
            ->setName('ip2location:load')
            ->setDescription('Loads csv file into the database (use: https://www.countryipblocks.net/country_selection.php to get subnets)')
            ->addOption('flush')
            ->addOption('ip2long', null, null, 'Convert the IP addresses into ip2long format before importing them.')
        ;
    }

    public function execute(InputInterface $input, OutputInterface $output) {

        $this->flush = $input->getOption('flush');
        $this->ip2long = $input->getOption('ip2long');

        $resource = $this->getContainer()->getParameter('ip2location.source');

        if(is_string($resource) && !realpath($resource)) {
            throw new \ErrorException("The provided path appear to be invalid (not accessible, perhaps?) \n\n". $resource);
        }
        else if(is_array($resource)) {
            if(count($resource) == 0){
                throw new \ErrorException('ip2location.source seems to be empty! Please set your ip2location.csv file or a path containing ip2location.csv files (to include them all) before running this command!');
            } else {
                $invalid = array();
                foreach($resource as $fileResource) {
                    if(!realpath($fileResource)){
                        $invalid[] = $fileResource;
                    }
                }
                if(count($invalid) > 0){
                    throw new \ErrorException("Some of the provided paths appear to be invalid (not accessible, perhaps?) \n\n". implode("\n", $invalid));
                }
            }
        }

        $this->doctrine = $this->getContainer()->get('doctrine');
        // Disable this to prevent RAM leaking.
        $this->doctrine->getConnection()->getConfiguration()->setSQLLogger(null);

        $totalLines = 0;

        if(is_string($resource) && is_dir($resource)) {
            $path = $resource;

            $resource = array();

            $finder = new Finder();
            $finder->files()->name('/\.(txt|csv)/')->in($path);

            /**
             * @var \SplFileInfo $file
             */
            foreach($finder as $file){
                $resource[] = $file->getRealPath();
            }
        }

        foreach($resource as $file) {
            $totalLines += $this->lineCount($file);
        }

        $progress = new ProgressBar($output, $totalLines);
        $progress->setOverwrite(true);
        $progress->setRedrawFrequency(283);
        $progress->setFormat('%current%/%max% [%bar%] %percent:3s%% %elapsed:6s%/%estimated:-6s% (RAM: %memory:6s%)');

        $progress->start();

        // Flush?
        if($this->flush) {
            $className = 'EdgeLabs\IP2LocationBundle\Entity\IP2Loc';

            $cmd = $this->doctrine->getManager()->getClassMetadata($className);

            $connection = $this->doctrine->getConnection();
            $dbPlatform = $connection->getDatabasePlatform();
            $connection->query('SET FOREIGN_KEY_CHECKS=0');
            $q = $dbPlatform->getTruncateTableSql($cmd->getTableName());
            $connection->executeUpdate($q);
            $connection->query('SET FOREIGN_KEY_CHECKS=1');
        }

        $inserted = 0;
        foreach($resource as $file){
            $inserted += $this->processCSVFile($file, $progress);
        }

        $progress->finish();

        $output->write("\n\nInserted ip addresses to database: ". $inserted."\n\n");

    }

    private function processCSVFile($csv_file, ProgressBar $progress) {
        $file = new SplFileObject($csv_file);
        $file->seek($file->getSize());
        $file->seek(0);

        // Set flags
        $file->setFlags(SplFileObject::READ_CSV | SplFileObject::SKIP_EMPTY | SplFileObject::READ_AHEAD | SplFileObject::DROP_NEW_LINE);

        // Process everything else
        $inserted = 0;
        $i = 0;
        $batchSize = 300;

        while (!$file->eof() && ($line = $file->fgetcsv()) != null) {
            $data = array(
                'ip_from'      => trim($line[0]),
                'ip_to'        => trim($line[1]),
                'country_code' => trim($line[2]),
                'country_name' => trim($line[3]),
                'locale'       => trim($line[4]),
            );

            if($this->ip2long){
                $data['ip_from'] = ip2long($data['ip_from']);
                $data['ip_to']   = ip2long($data['ip_to']);
            }

            if($i > 0) {
                $progress->advance();
            }

            $this->insert($data, $i, $batchSize);

            $inserted++;
            $i++;
        }
        // Take care of any leftovers...
        $this->doctrine->getManager()->flush();
        $this->doctrine->getManager()->clear();

        return $inserted;
    }

    private function insert(array $data, $i = 0, $batchSize = 300) {
        $ip = new IP2Loc();
        $ip->setIpFrom($data['ip_from']);
        $ip->setIpTo($data['ip_to']);
        $ip->setCountryCode($data['country_code']);
        $ip->setCountryName($data['country_name']);
        $ip->setLocale($data['locale']);

        $this->doctrine->getManager()->persist($ip);

        if (($i % $batchSize) === 0) {
            $this->doctrine->getManager()->flush();
            $this->doctrine->getManager()->clear();
        }

        return $i;
    }

    private function lineCount($file) {
        $file = new SplFileObject($file);
        $file->seek($file->getSize());
        return $file->key();
    }

}
